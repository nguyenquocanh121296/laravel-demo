<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee', function (Blueprint $table) {
            $table->bigIncrements('id');
						$table->string('first_name')->nullable(false);
						$table->string('last_name')->nullable(false);
						$table->string('birthday')->nullable(false);
						$table->string('gender')->nullable(true);
						$table->string('address')->nullable(true);
						$table->string('position')->nullable(true);
						$table->unsignedBigInteger('user_id');
						$table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee');
    }
}
