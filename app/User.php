<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Employee;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

		public function getJWTIdentifier()
		{
			return $this->getKey();
		}

		public function getJWTCustomClaims()
		{
			return [];
		}

		public function setPasswordAttribute($password)
		{
			if (!empty($password))
			{
				$this->attributes['password'] = bcrypt($password);
			};
		}

		public function employee()
		{
			return $this->hasOne(Employee::class, 'user_id', 'id');
		}

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'email_verified_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
