<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
	public function user()
	{
		return $this->belongsTo(User::class, 'user_id');
	}

	protected $fillable = [
		'first_name', 'last_name', 'birthday', 'gender', 'address', 'position',
	];

	protected $table = 'employee';
}
