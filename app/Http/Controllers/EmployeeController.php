<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Employee;
use App\User;
use Illuminate\Support\Facades\DB;
use Exception;
use App\Http\Controllers;
use Illuminate\Support\Facades\Auth;


class EmployeeController extends Controller
{


	public function index()
	{
		$employees = DB::table('employee')->select('first_name', 'last_name', 'birthday', 'address', 'gender', 'position')->get();

		return $this->render_json(true, $employees);
	}

	public function store(Request $request)
	{
		$validate = Validator::make($request->all(), $this->validate_rules());

		if($validate->errors()->isNotEmpty())
		{
			return $this->render_json(false, $validate->errors()->jsonSerialize(), 422);
		}

		try {
			DB::beginTransaction();
			$user = User::create($request->only('name', 'email','password'));

			$employee = new Employee([
				'first_name' => $request->input('employee.first_name'),
				'last_name' => $request->input('employee.last_name'),
				'birthday' => $request->input('employee.birthday'),
				'gender' => $request->input('employee.gender'),
				'address' => $request->input('employee.address'),
				'position' => $request->input('employee.position'),
			]);

			$user->employee()->save($employee);
			DB::commit();

			return $this->render_json(true, $user, 201);
		} catch (Exception $e){
			DB::rollBack();
			return $this->render_json(false, ["error" => $e], 422);
		}
	}

	public function update(Request $request)
	{
		$validate = Validator::make($request->all(),[
			'first_name' => 'required|max:255',
			'last_name' => 'required|max:255',
			'birthday' => 'date',
			'gender' => 'digits_between: 0, 2',
			'address' => 'max:255',
			'position' => 'max:255',
		]);

		if($validate->errors()->isNotEmpty())
		{
			return $this->render_json(false, $validate->errors()->jsonSerialize(), 422);
		}

		try {
			DB::beginTransaction();
			$employee = Employee::where("user_id", Auth::id())->first()->update([
				'first_name' => $request->input('first_name'),
				'last_name' => $request->input('last_name'),
				'birthday' => $request->input('birthday'),
				'gender' => $request->input('gender'),
				'address' => $request->input('address'),
				'position' => $request->input('position'),
			]);

			DB::commit();

			if($employee){
				return $this->render_json(true, ["data" => "Update employee for user success."]);
			} else {
				return $this->render_json(false, ["data" => "Update employee for user failed."], 400);
			}
		} catch (Exception $e){
			DB::rollBack();
			return $this->render_json(false, ["error" => $e], 422);
		}
	}

	public function destroy()
	{
		$user = User::find(Auth::id());
		$user->employee->delete();
		$user->delete();

		Auth::logout();
		return $this->render_json(true, ["data" => "Delete account success."], 200);
	}

	private function validate_rules()
	{
		return [
			'name' => 'required',
			'email' => 'required|email|unique:users|max: 255',
			'password' => 'required|min:6|confirmed',
			'employee.first_name' => 'required|max:255',
			'employee.last_name' => 'required|max:255',
			'employee.birthday' => 'date',
			'employee.gender' => 'digits_between: 0, 2',
			'employee.address' => 'max:255',
			'employee.position' => 'max:255',
		];
	}

	private function render_json($status = true, $data, $status_code = 200)
	{
		return response()->json([
			"success" => $status,
			"data" => $data,
		], $status_code);
	}
}
