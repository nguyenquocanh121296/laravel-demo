<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\User;

class AuthController extends Controller
{
	public function login()
	{
		$user = request(['email', 'password']);

		if(! $token = Auth::attempt($user))
		{
			return response()->json(['error' => 'Unauthorized'], 401);
		}

		return $this->respondWithToken($token);
	}

	public function me()
	{
		$me = User::find(Auth::id());
		$me->employee;

		return response()->json([
			"success" => true,
			"data" => [
				"user" => $me
			]
		]);
  }

	public function logout()
	{
		Auth::logout();

		return response()->json(['message' => 'Successfully logged out']);
	}

	public function refresh()
  {
     return $this->respondWithToken(Auth::refresh());
  }

	protected function respondWithToken($token)
   {
     return response()->json([
        'access_token' => $token,
        'token_type' => 'bearer',
        'expires_in' =>  3600,
     ]);
   }
}
