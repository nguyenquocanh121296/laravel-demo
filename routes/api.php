<?php

use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
	'middleware' => ['api', 'jwt.verify'],
], function( $router ) {
	Route::post('logout', 'AuthController@logout');
	Route::post('refresh', 'AuthController@refresh');
	Route::post('me', 'AuthController@me');
	Route::put('employee', 'EmployeeController@update');
	Route::delete('employee', 'EmployeeController@destroy');
});

Route::post('login', 'AuthController@login');
Route::get('employees', 'EmployeeController@index');
Route::post('employees', 'EmployeeController@store');

